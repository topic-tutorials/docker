# Docker common commands

# What is the repository for?

Records Docker common commands.


## Exercise.1


Uses "docker pull" to download nginx image.

Official Nginx image tags: https://hub.docker.com/_/nginx?tab=tags

> docker pull nginx:1.16

> docker images | grep nginx

> docker rmi nginx:1.16

> docker images | grep nginx

> docker image prune -f


## Exercise.2

Uses "docker run" run Nginx(web server) container.

### Docker container: Hello world 

> docker --version

> docker info

### https://github.com/docker-library/hello-world/blob/b715c35271f1d18832480bde75fe17b93db26414/amd64/hello-world/Dockerfile

> docker run hello-world

### Run nginx container and allow 8080 port to nginx port(80)
### Nginx Dockerfile: https://github.com/nginxinc/docker-nginx/blob/9a052e07b2c283df9960375ee40be50c5c462a7e/stable/stretch/Dockerfile 

> docker run --name willis-nginx -p 8080:80 nginx:1.16

### Use "docker ps" to confirm willis-nginx container status in another terminal.

> docker ps

> docker container ls
~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                  NAMES
817e984e4b87        nginx:1.16          "nginx -g 'daemon of…"   3 minutes ago       Up 2 minutes        0.0.0.0:8080->80/tcp   willis-nginx
~~~

### Try to remove container image.

> docker rmi nginx:1.16

~~~
Error response from daemon: conflict: unable to remove repository reference "nginx:1.16" (must force) - container e42a7777d8e8 is using its referenced image 18ff9f3390d6
~~~

### ctrl+c >> stop above container

> docker ps 

~~~
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
~~~

> docker ps -a

~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                     PORTS               NAMES
817e984e4b87        nginx:1.16          "nginx -g 'daemon of…"   4 minutes ago       Exited (0) 9 seconds ago                       willis-nginx
~~~

> docker rm willis-nginx 
> docker rm 817e984e4b87

> docker container prune -f 

> docker ps -a

~~~
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
~~~

## Exercise.3

Uses "docker run -v" mount host folder to Nginx container.


> docker container prune -f 

> sudo docker run --name willis-nginx -v /home/willis/Desktop/YilanUniversity/Class/Lesson1/Docker/docker/nginx/html:/usr/share/nginx/html:ro -p 8080:80 nginx:1.16
